SELECT * FROM customers WHERE country = "Philippines";

---------------------

SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName LIKE "%La Rochelle%";

----------------------

SELECT products.productName, products.MSRP FROM products WHERE productName = "The Titanic";

----------------------

SELECT employees.firstName, employees.lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

----------------------

SELECT customers.customerName FROM customers WHERE state IS NULL;

---------------------

SELECT employees.lastName, employees.firstName, employees.email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

----------------------

SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

----------------------

SELECT orders.customerNumber FROM orders WHERE comments LIKE "%DHL%";

---------------------

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

----------------------

SELECT DISTINCT customers.country FROM customers;

---------------------

SELECT customers.customerName, customers.country FROM customers WHERE country IN ("USA", "France", "Canada");

---------------------

SELECT employees.firstName, employees.lastName, offices.city FROM employees
    JOIN offices ON employees.officeCode = offices.officeCode WHERE employees.officeCode = 5;

--------------------

SELECT customers.customerName FROM customers    
    LEFT JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-------------------

SELECT products.productName, customers.customerName FROM products
    JOIN orderdetails ON products.productCode = orderdetails.productCode
    JOIN orders ON orderdetails.orderNumber = orders.orderNumber
    JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerName = "Baane Mini Imports";

---------------------

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers 
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    JOIN offices ON employees.officeCode = offices.officeCode
    GROUP BY country;

---------------------

SELECT products.productName, products.quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;

---------------------

SELECT customers.customerName FROM customers WHERE customers.phone LIKE "%+81%";